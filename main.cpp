/*
 * main.cpp
 *
 *  Created on: Nov 28, 2014
 *  Author: Luis A. Aguilera <laguiler@mail.usf.edu>
 * 	Driver program for the header file implementing and
 * 	defining functions in order to traverse a directed
 * 	graph
 */
#include "directedGraph.h"
#define START 0

int main()
{

   ofstream outClientFile("Aguilera.txt", ios::out);

   if(!outClientFile)
   {
      cerr << "File could not be opened." << endl;
      exit( 1 );
   }

   ifstream inClientFile( "input.txt", ios::in );

   if(!inClientFile)
   {
   	cerr << "File could not be opened." << endl;
   	exit( 1 );
   }


   vector <Digraph> inputGraphs;
   Digraph inGraph;

   unsigned int instances;

   inClientFile >> instances;

   cout << "There are: " << instances << " graphs to model" << endl;

   for(int i = 0; i < instances; ++i)
   {
      inClientFile >> inGraph;
   	inputGraphs.push_back(inGraph);
   }

   for(int i = 0; i < inputGraphs.size(); ++i)
   {
   	int destination = (inputGraphs[i].getSize()/2) - 1;

   	cout << "\nGraph number " << i+1 << ":\n" <<endl;

   	cout << "Destination is at position: " << destination << endl;

   	cout << "Searching for shortest path to the destination..." << endl;
   	vector<string> path = inputGraphs[i].breadthFirstSearch(START, destination);

   	cout << "Shortest path to the destination: ";
   	for(int j = 0; j < path.size(); ++j)
   	{
   			cout << path[j] << " ";
   			outClientFile << path[j] << " ";
   			if(j == path.size() - 1)
   			{
   				outClientFile << endl;
   			}
   	}
   	cout << endl;
   	cout << endl;
   }

   cout << "End of run." << endl;
   return 0;
}
