/*
 * directedGraph.h
 *
 *  Created on: Nov 28, 2014
 *  Author: Luis A. Aguilera <laguiler@mail.usf.edu>
 *
 * 	Header file implementing and defining functions
 * 	in order to traverse a directed graph
 */

#ifndef DIRECTEDGRAPH_H_
#define DIRECTEDGRAPH_H_

#include <iostream>
#include <vector>
#include <list>
#include <queue>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <string>
using namespace std;

/******************************************************************************
 * BIG HONKING NOTE:
 *
 * I don't know if it was the IDE I use (Eclipse), but for some reason, my
 * overloaded >> operator would -not- see class Vertex if it was a private
 * member of class Digraph, even though it sees vector<Vertex> vertices
 * just fine. I struggled with it for a few hours before I decided to go with
 * this implementation just to get everything to work
 *****************************************************************************/

/**'~** "Head nodes" of adjacency 1ists *****/
class Vertex
{
	public:

	int distance;
	list<int> neighbors;
};

class Digraph
{

   public:

	/***************************************************************************
	 * Empty constructor
	 * Precondition: None
	 * Postcondition: Creates an empty graph.
	 **************************************************************************/
	Digraph();

	/***************************************************************************
	 * Helper function to return the number of vertices in a graph
	 * Precondition: None
	 * Postcondition: Integer value giving the number of vertices in the graph
	 **************************************************************************/
	int getSize();

	/***************************************************************************
	 * Find a shortest path in the graph from vertex start to vertex destination
	 * Precondition: start and destination are vertices.
	 * Postcondition: A vector of directions along the shortest path from
	 * start to destination is returned.
	 **************************************************************************/
	vector <string> breadthFirstSearch(int start, int destination);

	/***************************************************************************
	 * Input operation.
	 * Precondition: ifstream inStream is open. The lines in the file to which
	 * it is connected are organized so that the first item denotes how many
	 * graphs we'll read in, followed by the number of rows/columns in the first
	 * graph, which in turn is followed by n * n vertices.
	 * Postcondition: The adjacency list representation of this digraph has been
	 * stored in myAdjacencyLists.
	 **************************************************************************/
	friend istream &operator>> ( istream &, Digraph & );

   private:

	/***** Data Member *****/
	vector<Vertex> vertices;
};

//--- Definition of Digraph()
Digraph::Digraph()
{

}

//--- Definition of getSize()
int Digraph::getSize()
{
	return vertices.size();
}

//--- Definition of vector<string> breadthFirstSearch(int, int)
vector<string> Digraph::breadthFirstSearch(int start, int destination)
{

	/***************************************************************************
	 * BIG HONKING NOTE:
	 *
	 * The majority of this section was adapted from the book's shortest path
	 * function and is not entirely my own work. I am placing this disclaimer
	 * here to avoid looking like I was trying to pass the book's work as my
	 * own. I understand if I will be deducted points for using the book
	 * instead of doing everything entirely on my own.
	 **************************************************************************/

	int n = vertices.size();
	int nValue = sqrt(n/2); // number of rows/columns
	int nValueSquared = nValue * nValue; // used for calculations

	//Create vectors to hold the distance from the start vertex
	//as well a vertex's parentVector.
	vector <int> distanceVector(n, -1);
	vector <int> parentVector(n);

	//Visit the start vertex.
	distanceVector[start] = 0;
	int distance = 0, vertex;

	//Initialize a queue to contain only the start vertex.
	queue<int> vertexQueue;
	vertexQueue.push(start);

	//While the queue is not empty
	while(distanceVector[destination] < 0 && !vertexQueue.empty())
	{

		vertex = vertexQueue.front();

		//Remove a vertex v from the queue
		vertexQueue.pop();

		if(distanceVector[vertex] > distance)
		{
			distance++;
		}

		//For all vertices w adjacent to v
		for(list<int>::iterator it = vertices[vertex].neighbors.begin();
				it != vertices[vertex].neighbors.end(); ++it)

			//If w has not been visited
			if(distanceVector[*it] < 0)
			{
				//Visit W
				distanceVector[*it] = distance + 1;
				parentVector[*it] = vertex;

				//Add w to the queue
				vertexQueue.push(*it);
			}

		++distance;
	}

	vector<int> path(distance+1);
	if(distanceVector[destination] < 0)
	{
		cout << "Destination not reachable from start vertex\n";
	}
	else
	{
		path[distance] = destination;
		for(int k = distance - 1; k >= 0; --k)
		{
			path[k] = parentVector[path[k+1]];
		}
	}

	//Something about my implementation of a graph did not gel with the book's
	//breadth first search. It would result in an abnormal amount of 0s being
	//added to the expected path. This is meant to remove all but one from the
	//path.
	while(path[1] == 0)
	{
		path.erase(path.begin()+1);
	}

	//Create a vector of strings to hold the directions we're to follow.
	vector<string> directions;

	//Loop through all values of vector<int> path with the exception of our goal
	//Insert a direction into our vector<string> directions. Directions are
	//calculated based on the difference between the current row/column and the
	//next row/column. Larger rows mean moving south, larger columns mean moving
	//east. The inverse is true for moving north and west. Diagonal movement is
	//a combination of these.
	for(int i = 0; i < path.size() - 1; ++i)
	{
		//Variables to determine the coordinates of our current vertex as well as
		//the next vertex. Needed to calculate how we're moving.
		int currentRow = path[i]/nValue;
		int currentColumn = path[i]%nValue;

		int nextRow = path[i+1]/nValue;
		int nextColumn = path[i+1]%nValue;

		//Movement from 0 through n^2-1 to n^2 through 2n^2-1 (from small to big)
		//is always diagonal
		if(path[i] < nValueSquared && path[i+1] >= nValueSquared)
		{
			if(nextRow > currentRow && nextColumn > currentColumn)
			{
				directions.push_back("SE");
			}

			else if(nextRow > currentRow && nextColumn < currentColumn)
			{
				directions.push_back("SW");
			}

			else if(nextRow < currentRow && nextColumn > currentColumn)
			{
				directions.push_back("NE");
			}

			else
			{
				directions.push_back("NW");
			}
		}

		//Movement that stays within n^2 through 2n^2-1 is always diagonal
		if(path[i] >= nValueSquared && path[i+1] >= nValueSquared)
		{
			if(nextRow > currentRow && nextColumn > currentColumn)
			{
				directions.push_back("SE");
			}

			else if(nextRow > currentRow && nextColumn < currentColumn)
			{
				directions.push_back("SW");
			}

			else if(nextRow < currentRow && nextColumn > currentColumn)
			{
				directions.push_back("NE");
			}

			else
			{
				directions.push_back("NW");
			}
		}

		//Movement that stays within 0 through n^2-1 is always horizontal
		if(path[i] < nValueSquared && path[i+1] < nValueSquared)
		{
			if(nextRow > currentRow)
			{
				directions.push_back("S");
			}

			else if(nextColumn < currentColumn)
			{
				directions.push_back("W");
			}

			else if(nextRow < currentRow)
			{
				directions.push_back("N");
			}

			else if(nextColumn > currentColumn)
			{
				directions.push_back("E");
			}
		}

		//Movement from n^2 through 2n^2-1 to 0 through n^2-1 (from big to small)
		//is always horizontal. There is one exception, however: If our current
		//vertex lies along the diagonal, and the next vertex is our end goal,
		//then movement is always southeast.
		if(path[i] >= nValueSquared && path[i+1] < nValueSquared) //horizontal
		{
			if(path[i+1] == nValueSquared-1 && currentRow == currentColumn)
			{
				directions.push_back("SE");
			}

			else
			{
				if(nextRow > currentRow/2)
				{
					directions.push_back("S");
				}

				else if(nextColumn < currentColumn/2)
				{
					directions.push_back("W");
				}

				else if(nextRow < currentRow/2)
				{
					directions.push_back("N");
				}

				else if(nextColumn > currentColumn/2)
				{
					directions.push_back("E");
				}
			}
		}
	}

	//Debugging: Checking the index value of each vertex
	/*for(int i = 0; i < path.size(); ++i)
	{
		cout << path[i] << " ";
	}

	cout << endl;*/

	return directions;
}

//--- Definition of istream&operator>>(istream &, Digraph &)
istream &operator>> ( istream &in, Digraph &targetGraph )
{
	//Clear our temporary graph's vertices.
	targetGraph.vertices.clear();

	//create a variable to store the dimensions of the n * n maze
	int nValue;

	//read the value from the input file and assign it to nValue
	in >> nValue;

	//create a vertex that will be copied into the graph's vector of vertices
	Vertex vertex;

	//create a data member that will hold the position of a vertex's neighbors
	int data;

	//expand the vector to fit 2n^2 vertices
	for(int i = 0; i < (2*(nValue * nValue)); ++i)
	{
		targetGraph.vertices.push_back(vertex);
	}

	//Read in the next n^2 values
	for(int i = 0; i < (nValue * nValue); ++i)
	{
		//assign the value read to the distance from the vertex to its neighbors
		//and create a temporary variable to use with our calculations
		in >> vertex.distance;
		int distance = vertex.distance;

		//create a temporary adjacency list to hold the position of the neighbors
		list<int> adjacencyList;

		//Instructions for handling positive values. Horizontal movement is
		//assigned first, followed by diagonal movement.
		if(distance > 0)
		{
			//If the row number minus the distance to the vertex's neighbors is
			//greater than or equal to zero, then traveling north is possible.
			if(((i/nValue) - distance) >= 0)
			{
				data = (i - (distance * nValue));
				adjacencyList.push_back(data);
			}

			//If the row number plus the distance to the vertex's neighbors is
			//less than nValue, then traveling south is possible.
			if(((i/nValue) + distance) < nValue)
			{
				data = (i + (distance * nValue));
				adjacencyList.push_back(data);
			}

			//If the column number minus the distance to the vertex's neighbors
			//is greater than or equal to zero, traveling west is possible.
			if(((i%nValue) - distance) >= 0)
			{
				data = (i - distance);
				adjacencyList.push_back(data);
			}

			//If the column number plus the distance to the vertex's neighbors is
			//less than nValue, then traveling east is possible.
			if(((i%nValue) + distance) < nValue)
			{
				data = (i + distance);
				adjacencyList.push_back(data);
			}

			//Copy the contents of our temporary list to the vertex's list
			vertex.neighbors = adjacencyList;

			//Horizontal movement happens with nodes in the 0 through (n^2 - 1)
			//vertices for positive values.
			targetGraph.vertices[i] = vertex;

			//Clear each list so they can be reused for vertical movement.
			adjacencyList.clear();
			vertex.neighbors.clear();

			//If northward and westward movement are both possible, then the
			//vertex has a northwestern neighbor.
			if((((i/nValue) - distance) >= 0) && (((i%nValue) - distance) >= 0))
			{
				data = (i - (distance * nValue) - distance + (nValue * nValue));
				adjacencyList.push_back(data);
			}

			//If northward and eastward movement are both possible, then the
			//vertex has a northeastern neighbor.
			if((((i/nValue) - distance) >= 0) && (((i%nValue) + distance)<nValue))
			{
				data = (i - (distance * nValue) + distance + (nValue * nValue));
				adjacencyList.push_back(data);
			}

			//If southward and westward movement are both possible, then the
			//vertex has a southwestern neighbor.
			if((((i/nValue) + distance)<nValue) && (((i%nValue) - distance) >= 0))
			{
				data = (i + (distance * nValue) - distance + (nValue * nValue));
				adjacencyList.push_back(data);
			}

			//If southward and eastward movement are both possible, then the
			//vertex has a southeastern neighbor.
			if((((i/nValue) + distance) < nValue) && (((i%nValue) + distance)
					< nValue))
			{
				//If we would land in space (2n^2)-1, instead land in space (n^2)-1
				if(((i + (distance * nValue) + distance + (nValue * nValue))
						- (nValue * nValue)) == ((nValue * nValue) -1))
				{
					data = (nValue * nValue) -1;
				}
				else
				{
					data = (i + (distance * nValue) + distance + (nValue * nValue));
				}
				adjacencyList.push_back(data);
			}

			//Diagonal movement happens with nodes in the n^2 through (2n^2 - 1)
			//vertices for positive values.
			vertex.neighbors = adjacencyList;
			targetGraph.vertices[i + (nValue * nValue)] = vertex;
		}

		//Instructions for handling negative values. Diagonal movement is
		//assigned first, followed by horizontal movement.
		if(distance < 0)
		{
			//If northward and westward movement are both possible, then the
			//vertex has a northwestern neighbor.
			if((((i/nValue) - abs(distance)) >= 0) && (((i%nValue) -
					abs(distance)) >= 0))
			{
				data = (i - (abs(distance) * nValue) - abs(distance) +
						(nValue * nValue));
				adjacencyList.push_back(data);
			}

			//If northward and eastward movement are both possible, then the
			//vertex has a northeastern neighbor.
			if((((i/nValue) - abs(distance)) >= 0) && (((i%nValue) +
					abs(distance)) < nValue))
			{
				data = (i - (abs(distance) * nValue) + abs(distance) +
						(nValue * nValue));
				adjacencyList.push_back(data);
			}

			//If southward and westward movement are both possible, then the
			//vertex has a southwestern neighbor.
			if((((i/nValue) + abs(distance)) < nValue) && (((i%nValue) -
					abs(distance)) >= 0))
			{
				data = (i + (abs(distance) * nValue) - abs(distance) +
						(nValue * nValue));
				adjacencyList.push_back(data);
			}

			//If southward and eastward movement are both possible, then the
			//vertex has a southeastern neighbor.
			if((((i/nValue) + abs(distance)) < nValue) && (((i%nValue) +
					abs(distance)) < nValue))
			{
				//If we would land in space (2n^2)-1, instead land in space (n^2)-1
				if(((i + (abs(distance) *nValue) + abs(distance) + (nValue*nValue))
						- (nValue * nValue)) == ((nValue * nValue) -1))
				{
					data = (nValue * nValue) -1;
				}
				else
				{
					data = (i + (abs(distance) * nValue) + abs(distance)
							+ (nValue * nValue));
				}
				adjacencyList.push_back(data);
			}

			//Diagonal movement happens with nodes in the 0 through n^2-1
			//vertices for negative values
			vertex.neighbors = adjacencyList;
			targetGraph.vertices[i] = vertex;

			//Clear each list so they can be reused for horizontal movement.
			adjacencyList.clear();
			vertex.neighbors.clear();

			//If the row number minus the distance to the vertex's neighbors is
			//greater than or equal to zero, then traveling north is possible.
			if(((i/nValue) - abs(distance)) >= 0)
			{
				data = (i - (abs(distance) * nValue));
				adjacencyList.push_back(data);
			}

			//If the row number plus the distance to the vertex's neighbors is
			//less than nValue, then traveling south is possible.
			if(((i/nValue) + abs(distance)) < nValue)
			{
				data = (i + (abs(distance) * nValue));
				adjacencyList.push_back(data);
			}

			//If the column number minus the distance to the vertex's neighbors
			//is greater than or equal to zero, traveling west is possible.
			if(((i%nValue) - abs(distance)) >= 0)
			{
				data = (i - abs(distance));
				adjacencyList.push_back(data);
			}

			//If the column number plus the distance to the vertex's neighbors is
			//less than nValue, then traveling east is possible.
			if(((i%nValue) + abs(distance)) < nValue)
			{
				data = (i + abs(distance));
				adjacencyList.push_back(data);
			}

			//Horizontal movement happens with nodes in the n^2 through (2n^2 - 1)
			//vertices for negative values
			vertex.neighbors = adjacencyList;
			targetGraph.vertices[i + (nValue * nValue)] = vertex;
		}

		//Assign end node no neighbors.
		if(distance == 0)
		{
			vertex.neighbors.clear();
			targetGraph.vertices[i] = vertex;
			targetGraph.vertices[i + (nValue * nValue)] = vertex;
		}
	}

	//Debugging: Making sure vertices and their neighbors
	//were assigned correctly.
	/*cout << "Vertices: ";
	for(int i = 0; i < targetGraph.vertices.size(); ++i)
	{
		cout << targetGraph.vertices[i].distance << " ";
	}
	cout << endl;*/

	/*cout << "-Neighbors of each vertex-" << endl;

	for(int i = 0; i < targetGraph.vertices.size(); ++i)
	{
		cout << "Vertex " << i << " : ";
		for(std::list<int>::iterator it=targetGraph.vertices[i].neighbors.begin();
		it != targetGraph.vertices[i].neighbors.end(); ++it)
		{
			cout << *it << " ";
		}
		cout << endl;
	}*/

	return in;
}
#endif /* DIRECTEDGRAPH_H_ */
